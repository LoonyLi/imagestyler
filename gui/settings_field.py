from PyQt5.QtCore import Qt, QObject, QStandardPaths, pyqtSlot, pyqtSignal
from PyQt5.QtGui import QStandardItemModel, QCursor
from PyQt5.QtWidgets import QHBoxLayout, QVBoxLayout, QLabel, QSlider, QDoubleSpinBox, QSpinBox, QComboBox, QWidget, \
                            QTabWidget, QCheckBox, QMenu, QAction, QFileDialog, QListWidget, QListWidgetItem, \
                            QTableWidgetItem, QGridLayout, QTableWidget, QFrame, QSizePolicy
from utils import init_type, vgg16_layers_list, getNetworkNames, initializeRange, getNamedWidget, items
import os

class StyleLayersWidget(QWidget):
    def __init__(self, parent=None):
        super(StyleLayersWidget, self).__init__(parent)

        self.style_layers_list = QListWidget()
        self.masks_list = QListWidget()
        self.mask_intensities_list = QListWidget()

        self.style_layers_list.setFrameStyle(QFrame.NoFrame);
        self.masks_list.setFrameStyle(QFrame.NoFrame);
        self.mask_intensities_list.setFrameStyle(QFrame.NoFrame);


class SettingsFieldWidget(QTabWidget):
    customImageChosen = pyqtSignal(['QString'])
    maskLoaded = pyqtSignal(['QString', int])
    maskIntensityChanged = pyqtSignal(int, int)
    
    styleLayerChecked = pyqtSignal(['QString'])
    styleLayerUnhecked = pyqtSignal(['QString'])

    def __init__(self, parent=None):
        super(SettingsFieldWidget, self).__init__(parent)

        self.first_tab = QWidget(self)
        self.second_tab = QWidget(self)
        self.third_tab = QWidget(self)

        self.name = QLabel("Settings")
        self.name.setAlignment(Qt.AlignHCenter | Qt.AlignVCenter);

        self.style_content_slider = QSlider(Qt.Horizontal)
        initializeRange(self.style_content_slider, 1, 200.0, 200.0, 1)
        self.style_content_slider.setTracking(False)

        self.tv_weight_box = QDoubleSpinBox()
        initializeRange(self.tv_weight_box, 0.01, 5.0, 1.0, 0.1)

        self.iters_box = QDoubleSpinBox()
        initializeRange(self.iters_box, 0.01, 100, 10, 0.01)

        self.optimization_iters_box = QSpinBox()
        initializeRange(self.optimization_iters_box, 1, 100, 20, 1)

        self.networks_list = QComboBox()
        self.content_layers_list = QComboBox()
        self.init_list = QComboBox()

        self.style_layers_list = QListWidget()
        self.masks_list = QListWidget()
        self.mask_intensities_list = QListWidget()

        self.style_layers_list.setFrameStyle(QFrame.NoFrame)
        self.masks_list.setFrameStyle(QFrame.NoFrame)
        self.mask_intensities_list.setFrameStyle(QFrame.NoFrame)

        self.style_layers_list.itemClicked.connect(lambda item: self.checkStyleLayerStatus(item))

        network_names = getNetworkNames()
        for name in network_names:
            self.networks_list.addItem(name)
        self.networks_list.setCurrentIndex(network_names.index('VGG16'))
        self.networks_list.currentTextChanged.connect(self.disableLayersSettings)

        self.set_layers_list(vgg16_layers_list)
        self.content_layers_list.setCurrentText('block2_conv2')

        for type in init_type:
            self.init_list.addItem(type)
        self.init_list.currentTextChanged.connect(self.custom_image_check)

        self.preserve_color_checkbox = QCheckBox()
        self.match_hist_checkbox = QCheckBox()

        self.masks_table = QTableWidget()
        self.masks_table.setColumnCount(3)
        self.masks_table.verticalHeader().setVisible(False)
        self.masks_table.cellDoubleClicked.connect(self.manageCells)
        self.masks_table.setContextMenuPolicy(Qt.CustomContextMenu)
        self.masks_table.customContextMenuRequested.connect(self.masksContextMenu)
        self.masks_table.setHorizontalHeaderLabels(('Style', 'Mask', 'Intensity'))

        network_widget        = getNamedWidget(self.networks_list, "Neural network to use:")
        content_layers_widget = getNamedWidget(self.content_layers_list, "Content layer to use:")
        match_hist_widget     = getNamedWidget(self.match_hist_checkbox, "Match histograms:", Qt.Horizontal)

        init_widget  = getNamedWidget(self.init_list, "Initialize generated image with:")
        ratio_widget = getNamedWidget(self.style_content_slider, "Style to content ratio:", Qt.Horizontal)
        tv_widget    = getNamedWidget(self.tv_weight_box, "Total variation weight:", Qt.Horizontal)
        iters_widget = getNamedWidget(self.iters_box, 
                                      "Iterations number (1..1000)\n OR min impovement (0.01..0.99):", Qt.Vertical)

        style_layers_list_widget  = getNamedWidget(self.style_layers_list, "Style layers:", Qt.Vertical)
        optimization_iters_widget = getNamedWidget(self.optimization_iters_box, 
                                                   "Iterations number for optimization function:", Qt.Vertical)
        preserve_color_widget     = getNamedWidget(self.preserve_color_checkbox, 
                                                   "Preserve content color:", Qt.Horizontal)

        middle_layout = QHBoxLayout()
        middle_layout.addWidget(match_hist_widget)
        middle_layout.addWidget(preserve_color_widget)

        middle_layout_2 = QHBoxLayout()
        middle_layout_2.addWidget(network_widget)
        middle_layout_2.addWidget(content_layers_widget)

        first_tab_layout = QVBoxLayout()
        first_tab_layout.addWidget(self.name)
        first_tab_layout.addWidget(ratio_widget)
        first_tab_layout.addWidget(tv_widget)
        first_tab_layout.addLayout(middle_layout)
        first_tab_layout.addLayout(middle_layout_2)
        first_tab_layout.addWidget(init_widget)
        self.first_tab.setLayout(first_tab_layout)

        second_tab_layout = QVBoxLayout()
        second_tab_layout.addWidget(style_layers_list_widget)
        second_tab_layout.addWidget(self.masks_table)
        self.second_tab.setLayout(second_tab_layout)

        third_tab_layout = QVBoxLayout()
        third_tab_layout.addWidget(iters_widget)
        third_tab_layout.addWidget(optimization_iters_widget)
        self.third_tab.setLayout(third_tab_layout)

        self.addTab(self.first_tab, "Common settings")
        self.addTab(self.second_tab, "Style layers")
        self.addTab(self.third_tab, "Optimization func")

        self.setMinimumSize(300, 300)
        self.setMaximumSize(300, 300)


    def custom_image_check(self, type):
        if type == init_type[3]:
            open_dir = QStandardPaths.standardLocations(QStandardPaths.DocumentsLocation)[0]
            file_types = ("Images (*.jpg *.png *.bmp)")
            name = QFileDialog.getOpenFileName(self, 'Choose image', directory=open_dir, filter=file_types)
            self.customImageChosen.emit(name[0])

    def set_layers_list(self, list):
        self.content_layers_list.clear()
        self.content_layers_list.addItems(list)

        self.style_layers_list.clear()
        self.style_layers_list.addItems(list)

        for item in items(self.style_layers_list):
            item.setFlags(item.flags() | Qt.ItemIsUserCheckable)
            item.setCheckState(Qt.Checked)

        self.enableLayersSettings()

    def addStylePath(self, path):
        item = QTableWidgetItem(os.path.basename(path))
        item.setFlags(item.flags() ^ Qt.ItemIsEditable)
        current_row = self.masks_table.rowCount()
        current_column = self.masks_table.columnCount()
        self.masks_table.setRowCount(current_row + 1)
        self.masks_table.setItem(current_row, 0, item)

    def clearStylePaths(self):
        self.masks_table.clear()
        self.masks_table.setRowCount(0)

    def disableLayersSettings(self):
        self.networks_list.setEnabled(False)
        self.content_layers_list.setEnabled(False)
        self.style_layers_list.setEnabled(False)

    def enableLayersSettings(self):
        self.networks_list.setEnabled(True)
        self.content_layers_list.setEnabled(True)
        self.style_layers_list.setEnabled(True)

    def load_mask(self, row, column):
        print("loading mask")
        open_dir = QStandardPaths.standardLocations(QStandardPaths.DocumentsLocation)[0]
        file_types = ("Images (*.jpg *.png *.bmp)")
        name = QFileDialog.getOpenFileName(self, 'Choose mask', directory=open_dir, filter=file_types)
        name = name[0]
        if name:
            item = QTableWidgetItem(os.path.basename(name))
            item.setFlags(item.flags() ^ Qt.ItemIsEditable)
            self.masks_table.setItem(row, column, item)
    
            intensity_field = QSpinBox()
            initializeRange(intensity_field, 1, 255, 255, 1)
            self.masks_table.setCellWidget(row, 2, intensity_field)
            intensity_field.valueChanged.connect(lambda value: self.maskIntensityChanged.emit(value, row))
    
            self.maskLoaded.emit(name, row)

    def manageCells(self, row, column):
        if column == 1:
            self.load_mask(row, column)

    def masksContextMenu(self, pos):
        menu = QMenu(self)
        menu.addAction(QAction("Clear"))
        menu.exec_(QCursor.pos())

    def checkStyleLayerStatus(self, layer_item):
        if layer_item.checkState() == Qt.Unchecked:
            self.styleLayerUnhecked.emit(layer_item.text())
        else:
            self.styleLayerChecked.emit(layer_item.text())