import numpy as np
import tensorflow as tf
import keras.backend as backend
import os
import time
import cv2
from scipy.optimize import minimize
import importlib.util
import tensorflow as tf
from PyQt5.QtCore import QObject, QThread, pyqtSignal
from utils import *
from threads import *

for network in getNetworkNames():
    spec = importlib.util.spec_from_file_location(network, os.path.join("networks", network + ".py"))
    module = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(module)
    globals()[network] = module


class LoadNetworkLayersThread(QThread): 
    finished = pyqtSignal('QStringList')

    def __init__(self, network_name, callback):
        super().__init__()
        self.network = None
        self.network_name = network_name
        self.finished.connect(callback)

    def run(self):
        initializeSession()
        self.network = getattr(globals()[self.network_name], self.network_name)
        model = self.network(include_top=False)
        layers = [layer.name for layer in model.layers if 'conv' in layer.name]
        self.finished.emit(layers)


class StylizationThread(QThread):   
    imageGenerated = pyqtSignal('QPixmap')
    generationCompleted = pyqtSignal()

    def __init__(self, params, image_callback, completed_callback):
        super().__init__()
        for key in params:
            setattr(self, key, params[key])

        self.loss = None
        self.gradients = None
        self.generated = None 

        self.imageGenerated.connect(image_callback)
        self.generationCompleted.connect(completed_callback)

    def run(self):
        initializeSession()

        self.loadContent()
        self.loadStyles()    
        self.generated = backend.placeholder((1, self.tensor_shape[1], self.tensor_shape[2], 3))  
        self.loadNetwork()

        layers = dict([(layer.name, layer.output) for layer in self.feature_extractor_network.layers])
        layer_shapes = dict([(layer.name, layer.output_shape) for layer in self.feature_extractor_network.layers])

        layer_features = layers[self.content_layer]  
        content_features = layer_features[0, :, :, :]
        generated_features = layer_features[self.tensors_num - 1, :, :, :]
        
        loss = backend.variable(0.)
        loss += self.content_weight * contentLoss(content_features, generated_features)

        styles_num = len(self.styles) 
        mask_paths = self.masks if self.masks else [None] * styles_num
        mask_intensities = self.mask_intensities if self.mask_intensities else [None] * styles_num  
            
        width = self.tensor_shape[2]
        height = self.tensor_shape[1]
        channels = self.tensor_shape[3]

        for layer_name in self.style_layers:
            layer_features = layers[layer_name]
            style_features = layer_features[1:self.tensors_num - 1, :, :, :]
            generated_features = layer_features[self.tensors_num - 1, :, :, :]
            
            layer_shape = layer_shapes[layer_name]
            style_losses = []
            for i in range(styles_num):
                l = style_loss(channels, width * height, style_features[i], generated_features, 
                               mask_paths[i], mask_intensities[i], layer_shape)
                style_losses.append(l) 
            loss += self.style_weight * backend.sum(style_losses) / len(self.style_layers)

        if self.total_variation_weight is not None:
           loss += self.total_variation_weight * totalVariationLoss(self.generated, width, height)

        outputs = [loss] + backend.gradients(loss, self.generated)
        self.f_outputs = backend.function([self.generated], outputs)

        self.generateInitialImage()

        print('Starting optimization')
        self.startOptimization()

        self.generated = postProcessImage(self.generated.reshape(self.tensor_shape).copy(), 
                                            self.preserve_content_color, self.match_histogram, 
                                            self.content_image)

        self.imageGenerated.emit(toPixmap(self.generated))
        print('Generation completed')
        self.generationCompleted.emit()

    def stop_callback(self):
        if self.isInterruptionRequested():
            raise RuntimeError("Terminated")

    def calculate_loss_and_gradients(self, image):
        outputs = self.f_outputs([image.reshape(self.tensor_shape).astype(backend.floatx())])
        self.loss = outputs[0]
        self.gradients = outputs[1].flatten().astype('float64')
    
    def loss_function(self, image):
        if self.loss == None:
            self.calculate_loss_and_gradients(image)
            loss = self.loss
        else:
            loss = np.copy(self.loss)
        self.loss = None
        return loss

    def gradient_function(self, image):
        if self.gradients is None:
            self.calculate_loss_and_gradients(image)
            gradients = self.gradients
        else:
            gradients = np.copy(self.gradients)
        self.gradients = None
        return gradients
    
    def iterate(self, iteration):
        print('Starting iteration: %d' % iteration)
        try:
            start_time = time.time()
            result = minimize(fun = self.loss_function, x0 = self.generated.flatten(),
                              jac = self.gradient_function, method = 'L-BFGS-B',
                              options = {'maxiter' : self.optimization_iterations},
                              callback = self.stop_callback())
            end_time = time.time()
    
            self.generated = result.x
            print('Current loss value: %d' % (result.fun))
            if self.old_loss is not None:
                self.diff = (self.old_loss - result.fun) / result.fun
                print('Difference percent: %f' % (self.diff))
            print('Time spent on %d iteration: %d sec' % (iteration, end_time - start_time))
            self.old_loss = result.fun
    
            pixmap = toPixmap(postProcessImage(self.generated.reshape(self.tensor_shape).copy(), 
                                                 self.preserve_content_color, self.match_histogram, 
                                                 self.content_image))
            self.imageGenerated.emit(pixmap)

        except RuntimeError:
            print("Generation interrupted")
            self.diff = None

    def loadContent(self):
        self.content_image = open_image(self.content_image_path)
        self.tensor_shape = (1, self.content_image.shape[0], self.content_image.shape[1], 3)
        self.preprocessed_content_image = preprocessImage(self.content_image, self.tensor_shape)
        self.content = backend.variable(self.preprocessed_content_image)

    def loadStyles(self):
        self.styles = []
        for image_path in self.style_image_paths:
            style_image = open_image(image_path)
            style_image = preprocessAndResize(style_image, self.tensor_shape)
            style_image = backend.variable(style_image)
            self.styles.append(style_image)

    def loadNetwork(self):
        print('Loading feature extractor network')
        tensors = [self.content]        
        for style_image_tensor in self.styles:
            tensors.append(style_image_tensor)
        tensors.append(self.generated)
        self.tensors_num = len(tensors)

        network = getattr(globals()[self.feature_extractor_network], self.feature_extractor_network)
        input_tensor = backend.concatenate(tensors, axis = 0) 
        self.feature_extractor_network = network(input_tensor = input_tensor, 
                                                 input_shape = self.content_image.shape, 
                                                 include_top = False)

    def generateInitialImage(self):
        if self.init_image == init_type[0]: 
            self.generated = self.preprocessed_content_image
        elif self.init_image == init_type[1]:
            first_style_image = open_image(self.style_image_paths[0])
            self.generated = preprocessAndResize(open_image(self.style_image_paths[0]), self.tensor_shape)
        elif self.init_image == init_type[2]:
            self.generated = preprocessImage(np.random.uniform(0, 256, size=self.tensor_shape))
        elif self.init_image == init_type[3]:
            self.generated = preprocessAndResize(open_image(self.init_image_path), self.tensor_shape)

        pixmap = toPixmap(postProcessImage(self.generated.reshape(self.tensor_shape).copy()))
        self.imageGenerated.emit(pixmap)

    def startOptimization(self):
        self.old_loss = None
        self.diff = 1
        if self.min_improvement is not None:
            print('Transfer will be performed until the improvement is higher than: {}%'.format(self.min_improvement*100))
            iteration = 0
            while self.diff > self.min_improvement:
                self.iterate(iteration)
                iteration += 1
                if not self.diff:
                    return
        else:
            print('Transfer will be performed with {} iterations'.format(self.iterations))
            for iteration in range(self.iterations):
                self.iterate(iteration)
                if not self.diff:
                    return

