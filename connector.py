from PyQt5.QtWidgets import QApplication, QMainWindow, QHBoxLayout, \
 QVBoxLayout, QWidget, QPushButton, QLabel, QSizePolicy
from PyQt5.QtCore import QObject, QEvent, Qt,  QThread, pyqtSignal
from PyQt5.QtGui import QPixmap

class Connector(QObject):   
    def __init__(self, styler, app_window, parent = None):        
        super(Connector, self).__init__(parent)

        self.styler = styler
        self.widget = app_window.app_widget
        self.settings = self.widget.settings_field

        self.widget.generated_img_filter = MouseClickFilter()
        self.widget.generated_img_field.images_list.viewport() \
            .installEventFilter(self.widget.generated_img_filter)

        self.widget.style_img_filter = StyleImagesDropFilter()
        self.widget.style_img_field.images_list \
            .installEventFilter(self.widget.style_img_filter)

        self.widget.content_img_filter = ContentImageDropFilter()
        self.widget.content_img_field.images_list \
            .installEventFilter(self.widget.content_img_filter)

        self.widget.generated_img_filter.leftButtonClicked \
            .connect(self.startStylization)
        self.styler.generationStarted.connect(self.widget.disable)

        self.widget.style_img_filter.styleImageDropped \
            .connect(self.styler.addStylePath)
        self.widget.style_img_filter.styleImageDropped \
            .connect(self.settings.addStylePath)
        self.widget.style_img_filter.styleImageDropped  \
            .connect(self.widget.style_img_field.appendImage)

        self.widget.content_img_filter.contentImageDropped \
            .connect(self.styler.setContentPath)
        self.widget.content_img_filter.contentImageDropped \
            .connect(self.widget.content_img_field.setImage)

        self.widget.content_img_field.imagesRemoved \
            .connect(self.styler.setContentPath)
        self.widget.style_img_field.imagesRemoved \
            .connect(self.styler.clearStylePaths)
        self.widget.style_img_field.imagesRemoved \
            .connect(self.settings.clearStylePaths)

        self.settings.tv_weight_box.valueChanged \
            .connect(self.styler.setTotalVariationWeight)
        self.settings.style_content_slider.valueChanged \
            .connect(self.styler.setContentStyleRatio)

        self.settings.preserve_color_checkbox.stateChanged \
            .connect(self.styler.setContentColorPreserving)
        self.settings.match_hist_checkbox.stateChanged \
            .connect(self.styler.setHistogramMatching)

        self.settings.init_list.currentTextChanged \
            .connect(self.styler.setInitImage)
        self.settings.customImageChosen \
            .connect(self.styler.setInitImagePath)

        self.settings.networks_list.currentTextChanged \
            .connect(self.styler.setFeatureExtractorNetwork)
        self.styler.networkLayersLoaded \
            .connect(self.settings.set_layers_list)

        self.settings.iters_box.valueChanged \
            .connect(self.styler.setIterationsNumber)
        self.settings.optimization_iters_box.valueChanged \
            .connect(self.styler.setOptimizationIterationsNumber)

        self.styler.imageGenerated \
            .connect(self.widget.generated_img_field.setImage)
        self.styler.generationCompleted \
            .connect(self.widget.enable)

        self.widget.interruptRequested \
            .connect(self.styler.interrupt)

        self.settings.maskLoaded \
            .connect(self.styler.setMask)
        self.settings.maskIntensityChanged  \
            .connect(self.styler.setMaskIntensity)

        self.settings.content_layers_list.currentTextChanged \
            .connect(self.styler.setContentLayer)
        self.settings.styleLayerChecked  \
            .connect(self.styler.addStyleLayer)
        self.settings.styleLayerUnhecked \
            .connect(self.styler.removeStyleLayer)

    def startStylization(self):
        if self.widget.generated_img_field.imageSet():
            print("Right click to clear field for generated image & left click to start again")
        else:
            self.styler.stylize()

class MouseClickFilter(QObject):
    leftButtonClicked = pyqtSignal()

    def __init__(self, parent = None):
        super(MouseClickFilter, self).__init__(parent)

    def eventFilter(self, object, event):
        if event.type() == QEvent.MouseButtonPress:
            if event.buttons() & Qt.LeftButton:
                self.leftButtonClicked.emit()
                return True
        return QObject.eventFilter(self, object, event);


class StyleImagesDropFilter(QObject):
    styleImageDropped = pyqtSignal('QString')

    def __init__(self, parent = None):
        super(StyleImagesDropFilter, self).__init__(parent)

    def eventFilter(self, object, event):
        if event.type() == QEvent.DragEnter:
            if event.mimeData().hasUrls():
                event.setDropAction(Qt.CopyAction)
                event.acceptProposedAction()  
                return True
            else:
                event.ignore()

        if event.type() == QEvent.Drop:
            for url in event.mimeData().urls():
                url = url.toLocalFile()
                if QPixmap(url).isNull():
                    event.ignore()
                    print("Not an image: {}".format(url))
                else:
                    print(url)
                    self.styleImageDropped.emit(url)
            return True
      
        return QObject.eventFilter(self, object, event)


class ContentImageDropFilter(QObject):
    contentImageDropped = pyqtSignal('QString')

    def __init__(self, parent = None):
        super(ContentImageDropFilter, self).__init__(parent)

    def eventFilter(self, object, event):
        if event.type() == QEvent.DragEnter:
            if event.mimeData().hasUrls():
                event.setDropAction(Qt.CopyAction)
                event.acceptProposedAction()  
                return True
            else:
                event.ignore()

        if event.type() == QEvent.Drop:
            if len(event.mimeData().urls()) > 1:
                event.ignore()
                print("More than one file is not allowed to drop")
            else:
                url = event.mimeData().urls()[0].toLocalFile()
                if QPixmap(url).isNull():
                    event.ignore()
                    print("Not an image: {}".format(url))
                else:
                    self.contentImageDropped.emit(url)
                return True
      
        return QObject.eventFilter(self, object, event)
