from PyQt5.QtCore import Qt, QObject, pyqtSignal
from PyQt5.QtGui import QTextCursor, QKeySequence 
from PyQt5 import QtGui
from PyQt5.QtWidgets import QApplication, QMainWindow, QHBoxLayout, QVBoxLayout, \
                            QWidget, QPushButton, QLabel, QSizePolicy, QTextEdit
from gui.image_field import ImageFieldWidget
from gui.settings_field import SettingsFieldWidget
from utils import handle_exception
import sys
import os.path
import traceback

from utils import LogStream
from connector import Connector
from image_styler import ImageStyler

sys.excepthook = handle_exception

class AppWidget(QWidget):
    interruptRequested = pyqtSignal()

    def __init__(self, parent):        
        super(AppWidget, self).__init__(parent)

        self.content_img_field = ImageFieldWidget("Content")
        self.style_img_field = ImageFieldWidget("Style")
        self.generated_img_field = ImageFieldWidget("Stylize it!")
        
        self.content_img_filter = None
        self.style_img_filter = None
        self.generated_img_filter = None

        self.generated_img_field.setMinimumHeight(256)

        self.logs_label = QTextEdit()
        self.logs_label.setReadOnly(True)
        self.logs_label.setStyleSheet("QLabel { background-color : #e5e5e5; }")
        self.logs_label.setMinimumHeight(30)

        self.settings_field = SettingsFieldWidget()

        self.main_layout = QVBoxLayout()
        self.h_layout = QHBoxLayout()

        self.left_layout = QVBoxLayout()
        self.left_layout.addWidget(self.content_img_field)
        self.left_layout.addWidget(self.style_img_field)

        self.right_layout = QVBoxLayout()
        self.right_layout.addWidget(self.settings_field)
        self.right_layout.addWidget(self.generated_img_field)

        self.h_layout.addLayout(self.left_layout)
        self.h_layout.addLayout(self.right_layout)

        self.main_layout.addLayout(self.h_layout)
        self.main_layout.addWidget(self.logs_label)

        self.setLayout(self.main_layout)
        
        sys.stdout = LogStream(logWritten = self.writeLog)
        sys.stderr = LogStream(logWritten = self.writeLog)

    def writeLog(self, log):
        self.logs_label.insertPlainText(log) 
        self.logs_label.ensureCursorVisible() 

    def disable(self):
        self.settings_field.setEnabled(False)

        self.content_img_field.images_list.removeEventFilter(self.content_img_filter)
        self.style_img_field.images_list.removeEventFilter(self.style_img_filter)
        self.generated_img_field.images_list.viewport().removeEventFilter(self.generated_img_filter)

    def enable(self):
        self.settings_field.setEnabled(True)

        self.content_img_field.images_list.installEventFilter(self.content_img_filter)
        self.style_img_field.images_list.installEventFilter(self.style_img_filter)
        self.generated_img_field.images_list.viewport().installEventFilter(self.generated_img_filter)

    def keyPressEvent(self, event):
        if QKeySequence(event.key()+int(event.modifiers())) == QKeySequence("Ctrl+C"):
            self.interruptRequested.emit()


class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.app_widget = AppWidget(self) 
        self.setCentralWidget(self.app_widget) 

if __name__ == '__main__':
    app = QApplication(sys.argv)
    app.setApplicationName("Image Styler")

    mainWin = MainWindow()
    styler = ImageStyler()
    connector = Connector(styler, mainWin)

    mainWin.show()
    sys.exit(app.exec_())