from utils import *
from threads import *

class ImageStyler(QObject):
    generationStarted = pyqtSignal()
    generationCompleted = pyqtSignal()
    networkLayersLoaded = pyqtSignal('QVariantList')
    imageGenerated = pyqtSignal('QPixmap')

    def __init__(self, parent = None):   
        super(ImageStyler, self).__init__(parent)
        self.stylization_thread = None
        self.stylized_image = None

        self.init_image = init_type[0]
        self.init_image_path = None
        self.feature_extractor_network = 'VGG16'
        self.content_layer = 'block2_conv2'
        self.style_layers = vgg16_layers_list
        self.content_image_path = []
        self.style_image_paths = []
        self.content_weight = 0.025
        self.style_weight = 5.0
        self.total_variation_weight = 1.0
        self.masks = {}
        self.mask_intensities = {}
        self.preserve_content_color = False
        self.match_histogram = False
        self.iterations = 10
        self.min_improvement = None
        self.optimization_iterations = 20

    def onImageGenerated(self, image):
        self.imageGenerated.emit(image) 

    def onGenerationCompleted(self):
        self.generationCompleted.emit() 

    def stylize(self):
        params = {
            "init_image" : self.init_image,
            "init_image_path" : self.init_image_path,
            "feature_extractor_network" : self.feature_extractor_network,
            "content_layer" : self.content_layer,
            "style_layers" : self.style_layers,
            "content_weight" : self.content_weight,
            "style_weight" : self.style_weight,
            "total_variation_weight" : self.total_variation_weight,
            "masks" : self.masks,
            "mask_intensities" : self.mask_intensities,
            "preserve_content_color" : self.preserve_content_color,
            "match_histogram" : self.match_histogram,
            "iterations" : self.iterations,
            "optimization_iterations" : self.optimization_iterations,
            "min_improvement" : self.min_improvement,
            "style_image_paths" : self.style_image_paths,
            "content_image_path" : self.content_image_path
        }
        if not self.style_image_paths or not self.content_image_path:
            print("Style/content image paths are not set!")
        elif not self.style_layers:
            print("No style layers were chosen!")
        else:
            self.generationStarted.emit()
            self.stylization_thread = StylizationThread(params, 
                                                        image_callback=self.onImageGenerated, 
                                                        completed_callback=self.onGenerationCompleted)
            self.stylization_thread.start()

    def interrupt(self):
        if self.stylization_thread:
            self.stylization_thread.requestInterruption()

    def setContentPath(self, path = None):
        self.content_image_path = path
        print("Content image path: {}".format(self.content_image_path))

    def addStylePath(self, path = None):
        self.style_image_paths.append(path)
        print("Style image path added: {}".format(path))

    def clearStylePaths(self):
        self.style_image_paths.clear()

    def setContentStyleRatio(self, ratio):
        self.content_weight = (201 - ratio) / 40
        self.style_weight = ratio / 40
        print("Content weight: {}; style weight: {}".format(self.content_weight, self.style_weight))

    def setTotalVariationWeight(self, weight):
        self.total_variation_weight = weight
        print("Total variation weight: {}".format(self.total_variation_weight))

    def onLayersLoaded(self, layers):
        self.style_layers.clear()
        self.style_layers = layers
        self.networkLayersLoaded.emit(layers)

    def setFeatureExtractorNetwork(self, network_name):
        self.feature_extractor_network = network_name
        print("Feature extractor network: {}".format(network_name))
        self.load_thread = LoadNetworkLayersThread(network_name, callback=self.onLayersLoaded)
        self.load_thread.start()

    def setContentLayer(self, layer):
        self.content_layer = layer
        print("Content layer: {}".format(layer))

    def addStyleLayer(self, layer):
        if not layer in self.style_layers:
            self.style_layers.append(layer)
            print("Style layer added: {}".format(layer))

    def removeStyleLayer(self, layer):
        if layer in self.style_layers:
            self.style_layers.remove(layer)
            print("Style layer removed: {}".format(layer))

    def setMask(self, mask, style_index):
        if style_index not in self.masks.keys():
            self.masks.update({style_index:mask})
            self.mask_intensities.update({style_index:255})
        else:
            self.masks[style_index] = mask
        print(self.masks)
        print(self.mask_intensities)

    def setMaskIntensity(self, intensity, style_index):
        self.mask_intensities[style_index] = intensity
        print(self.mask_intensities)

    def setContentColorPreserving(self, preserve_content_color):
        self.preserve_content_color = True if preserve_content_color != 0 else False
        print("Preserve content color: {}".format(preserve_content_color))

    def setHistogramMatching(self, match_histogram):
        self.match_histogram = match_histogram
        print("Match histograms: {}".format(self.match_histogram))

    def setMinImprovement(self, threshold):
        self.min_improvement = threshold
        print("Minimum improvement: {}".format(threshold))

    def setIterationsNumber(self, number):
        if number < 1:
            self.setMinImprovement(number)
        else:
            self.iterations = int(number)
            print("Iterations: {}".format(self.iterations))

    def setOptimizationIterationsNumber(self, number):
        self.optimization_iterations = number
        print("Optimization iterations: {}".format(self.optimization_iterations))

    def setInitImage(self, init_image):
        self.init_image = init_image
        print("Init image: {}".format(self.init_image))

    def setInitImagePath(self, init_image_path):
        self.init_image_path = init_image_path
        print("Init image path: {}".format(self.init_image_path))
        

