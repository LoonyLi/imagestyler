from os.path import basename, splitext
import glob
import sys
import numpy as np
import cv2
import keras.backend as backend
import tensorflow as tf
from keras.backend.tensorflow_backend import set_session 
from keras.applications.imagenet_utils import preprocess_input 
from PyQt5.QtCore import Qt, QObject, pyqtSignal, QFile, QFileDevice
from PyQt5.QtGui import QPixmap, QImage
from PyQt5.QtWidgets import QVBoxLayout, QHBoxLayout, QWidget, QLabel
import os.path
import traceback
from time import sleep

def handle_exception(exc_type, exc_value, exc_traceback):
  if issubclass(exc_type, KeyboardInterrupt):
    return
  print("An error occured:")
  print(traceback.format_exception(exc_type, exc_value, exc_traceback))
  sleep(3)
  sys.exit(1)

init_type = ['Content image', 'Style image', 'Random noise', 'Custom image']

vgg16_layers_list = ['block1_conv1', 'block1_conv2', 'block2_conv1', 'block2_conv2', 
                     'block3_conv1', 'block3_conv2', 'block3_conv3', 'block4_conv1', 
                     'block4_conv2', 'block4_conv3', 'block5_conv1', 'block5_conv2', 
                     'block5_conv3']

class LogStream(QObject):
    logWritten = pyqtSignal(str)

    def write(self, log):
        self.logWritten.emit(str(log))
        
    def flush(self):
        pass

def initializeSession():
    config = tf.ConfigProto()  
    config.gpu_options.allow_growth = True  
    set_session(tf.Session(config=config))
         
def getNetworkNames():
    return [basename(splitext(path)[0]) for path in glob.glob("networks/*.py")]


def preprocessImage(image, shape=None):
    if shape:
        image = image.reshape(shape).astype(backend.floatx())
    return preprocess_input(image)


def preprocessAndResize(image, tensor_shape):
    if (image.shape[0] != tensor_shape[1]) or (image.shape[1] != tensor_shape[2]):
        image = cv2.resize(image, dsize=(tensor_shape[2], tensor_shape[1]),    
                           interpolation=cv2.INTER_AREA)
    return preprocessImage(image, tensor_shape)


def postProcessImage(image, preserve_content_color=False, match_histogram=False, content_image=None):
    image[:, :, :, 0] += 103.939
    image[:, :, :, 1] += 116.779
    image[:, :, :, 2] += 123.68
    image = np.clip(image[:, :, :, ::-1], 0, 255).astype('uint8')[0]
    if preserve_content_color or match_histogram:
        image = matchColors(content_image, image, match_histogram, preserve_content_color)
    return image


def gramMatrix(filters):
    features = backend.batch_flatten(backend.permute_dimensions(filters, (2, 0, 1)))
    return backend.dot(features, backend.transpose(features))


def style_loss(channels, size, style, generated, mask_paths=None, mask_intensity=None, layer_shape=None):
    if mask_paths is not None:
        mask = backend.variable(loadMask(mask_paths, mask_intensity, layer_shape))
        style = style * backend.stop_gradient(mask)
        generated = generated * backend.stop_gradient(mask)
        del mask
    S = gramMatrix(style)
    G = gramMatrix(generated)
    return backend.sum(backend.square(S - G)) / ((size ** 2) * (channels ** 2) * 4.0)


def contentLoss(content, generated):
    return backend.sum(backend.square(generated - content))


def totalVariationLoss(x, width, height):
    w_var = backend.square(x[:, :height-1, :width-1, :] - x[:, 1:, :width-1, :])
    h_var = backend.square(x[:, :height-1, :width-1, :] - x[:, :height-1, 1:, :])
    return backend.sum(w_var + h_var)


def matchHistograms(stylized, content):
    shape = stylized.shape

    content = content.ravel()
    content_pixels, content_counts = np.unique(content, return_counts=True)
    content_hist_values = np.cumsum(content_counts).astype(np.float64)
    content_hist_values /= content_hist_values[-1]

    stylized = stylized.ravel()
    stylized_pixels, indices, stylized_counts = np.unique(stylized, return_counts=True, return_inverse=True)
    stylized_hist_values = np.cumsum(stylized_counts).astype(np.float64)
    stylized_hist_values /= stylized_hist_values[-1]

    interpolated = np.interp(stylized_hist_values, content_hist_values, content_pixels)
    return np.array(interpolated[indices].reshape(shape)).astype(np.uint8)


def matchColors(content, stylized, match_histogram=False, preserve_content_color=False):
    content = cv2.cvtColor(content, cv2.COLOR_BGR2YUV)
    stylized = cv2.cvtColor(stylized, cv2.COLOR_BGR2YUV)
    
    if preserve_content_color:
        c1_s, c2_s, c3_s = cv2.split(stylized)
        c1_c, c2_c, c3_c = cv2.split(content)
    else:
        c1_s, c2_s, c3_s = cv2.split(stylized)
        c1_c, c2_c, c3_c = cv2.split(content)
    
    if match_histogram:
        c1_s = matchHistograms(c1_s, c1_c)
    
    merged = cv2.merge((c1_s, c2_c, c3_c))
    return cv2.cvtColor(merged, cv2.COLOR_YUV2BGR)


def loadMask(mask_path, mask_intensity, shape):
    _, width, height, channels = shape

    mask = cv2.imread(mask_path, cv2.IMREAD_GRAYSCALE)
    mask = cv2.resize(mask, (height, width)).astype('float32')

    mask[mask <= 127] = 0
    mask[mask > 128] = mask_intensity

    mask /= np.amax(mask)
    mask_shape = shape[1:]
    mask_tensor = np.empty(mask_shape)

    for i in range(channels):
        mask_tensor[:, :, i] = mask
    return mask_tensor 


def toPixmap(image_array):
    cv2.cvtColor(image_array, cv2.COLOR_BGR2RGB, image_array)
    height, width, _ = image_array.shape
    bytesPerLine = 3 * width
    qimage = QImage(image_array, width, height, bytesPerLine, QImage.Format_RGB888)
    return QPixmap(qimage)


def initializeRange(widget, min, max, default, step):
    widget.setRange(min, max)
    widget.setSingleStep(step)
    widget.setValue(default)


def getNamedWidget(widget, name, name_position=Qt.Vertical):
    main_widget = QWidget()
    if name_position == Qt.Vertical:
        layout = QVBoxLayout(main_widget)
    else: 
        layout = QHBoxLayout(main_widget)

    name_label = QLabel(name)
    layout.addWidget(name_label)
    layout.addWidget(widget)
    main_widget.setLayout(layout)
    return main_widget


def items(list_widget):
    for i in range(list_widget.count()):
        yield list_widget.item(i)

def open_image(path):
    stream = open(path, "rb")
    bytes = bytearray(stream.read())
    arr = np.asarray(bytes, dtype=np.uint8)
    image = cv2.imdecode(arr, cv2.IMREAD_UNCHANGED)
    if len(image.shape) > 2 and image.shape[2] == 4:
        image = cv2.cvtColor(image, cv2.COLOR_BGRA2BGR)
    return image