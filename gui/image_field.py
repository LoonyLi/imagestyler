from PyQt5.QtCore import QVariant, QUrl, Qt, QObject, QStandardPaths, pyqtSlot, pyqtSignal, QRect, QSize
from PyQt5.QtGui import QPixmap, QIcon
from PyQt5.QtWidgets import QHBoxLayout, QLabel, QWidget, QSizePolicy, QMenu, \
                            QAction, QFileDialog, QGridLayout, QListWidget, \
                            QListWidgetItem

class ImageFieldWidget(QWidget):
    imagesRemoved = pyqtSignal()
    maskLoaded = pyqtSignal(['QString'])

    def __init__(self, text, parent=None):
        super(ImageFieldWidget, self).__init__(parent)

        self.setAcceptDrops(True)
        self.setFixedSize(300, 300)

        self.images_list = QListWidget()
        self.images_list.setViewMode(QListWidget.IconMode)
        self.images_list.setIconSize(QSize(271, 271))
        self.current_pixmap = None

        self.text = text
        self.text_item = QListWidgetItem(text)
        self.text_item.setSizeHint(QSize(271, 271))
        self.text_item.setTextAlignment(Qt.AlignCenter)
        self.text_item.setFlags(self.text_item.flags() \
                                & ~Qt.ItemIsSelectable & ~Qt.ItemIsDragEnabled \
                                & ~Qt.ItemIsDropEnabled & ~Qt.ItemIsUserCheckable)

        self.images_list.setStyleSheet("QListWidget::item:hover { background: transparent; }\
                                        QListWidget::item:disabled { color: transparent; \
                                                                     background-color: transparent; \
                                                                     background: transparent; }\
                                        QListWidget::item:hover:!active { background: transparent; }\
                                        QListWidget { background-color : #ffcccc; }")
        self.images_list.addItem(self.text_item)

        self.layout = QHBoxLayout(self)
        self.layout.addWidget(self.images_list)
        self.setLayout(self.layout)

        self.clear_field_action = QAction("Clear")
        self.clear_field_action.triggered.connect(lambda: self.clear_field())

        self.save_image_action = QAction("Save")
        self.save_image_action.triggered.connect(lambda: self.save_image())

    def clear_field(self):
        self.images_list.clear()
        self.images_list.addItem(self.text_item)
        self.imagesRemoved.emit()
        self.current_pixmap = None

    def save_image(self):
        save_dir = QStandardPaths.standardLocations(QStandardPaths.DocumentsLocation)[0]
        file_types = ("Images (*.jpg *.png *.bmp)")
        name = QFileDialog.getSaveFileName(self, 'Save image', directory=save_dir, filter=file_types)
        succsess = self.current_pixmap.save(name[0])
        if succsess:
            print("Image saved")
        else:
            print("Saving failed")

    def imageToItem(self, pixmap):
        item = QListWidgetItem(QIcon(pixmap), None)
        item.setSizeHint(QSize(271, 271))
        item.setTextAlignment(Qt.AlignCenter)
        item.setFlags(item.flags() & ~Qt.ItemIsSelectable & ~Qt.ItemIsDragEnabled \
                                   & ~Qt.ItemIsDropEnabled & ~Qt.ItemIsUserCheckable)
        return item

    def removeText(self):
        index = self.images_list.indexFromItem(self.text_item)
        if index.row() > -1:
            self.text_item = self.images_list.takeItem(index.row())

    def appendImage(self, url):
        self.removeText()
        self.current_pixmap = QPixmap(url)
        self.images_list.addItem(self.imageToItem(self.current_pixmap))

    def setImage(self, url):
        self.removeText()
        self.images_list.clear()
        self.appendImage(url)

    def contextMenuEvent(self, event):
        if self.images_list:
            menu = QMenu(self)
    
            menu.addAction(self.save_image_action)
            menu.addAction(self.clear_field_action)
    
            menu.exec_(self.mapToGlobal(event.pos()))

    def imageSet(self):
        return True if self.current_pixmap else False